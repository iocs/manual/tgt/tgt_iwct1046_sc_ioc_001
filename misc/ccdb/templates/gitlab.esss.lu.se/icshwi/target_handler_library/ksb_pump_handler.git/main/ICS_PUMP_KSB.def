###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##                                                                                          ##  
##                                  Control Pump (KSB DRIVE)                           		##
##                                                                                          ##  
##                                                                                          ##  
############################         Version: 1.0             ################################
# Author:  Attila Horvath, Marino Vojneski
# Date:    2022-09-05
# Version: v1.0
##############################################################################################


############################
#  STATUS BLOCK
############################
define_status_block()

#Operation modes
add_digital("OpMode_Auto",      PV_DESC="Operation Mode Auto",      PV_ONAM="True",		PV_ZNAM="False",  ARCHIVE=True)
add_digital("OpMode_Manual",    PV_DESC="Operation Mode Manual",    PV_ONAM="True",     PV_ZNAM="False",  ARCHIVE=True)
add_digital("OpMode_Forced",    PV_DESC="Operation Mode Forced",    PV_ONAM="True",     PV_ZNAM="False",  ARCHIVE=True)

#Pump states
add_analog("PumpSpeed",         "REAL",     PV_DESC="Pump speed AI in percentage",  PV_EGU="%",     ARCHIVE=True)
add_analog("PumpSetpoint",      "REAL",     PV_DESC="Pump Setpoint",                PV_EGU="%",     ARCHIVE=True)
add_analog("PumpSetpoint_Cmd",  "REAL",     PV_DESC="Pump Control AO",              PV_EGU="%",     ARCHIVE=True)
add_analog("PumpOutSpeed",      "REAL",     PV_DESC="Pump Actual Speed in rpm",     PV_EGU="rpm",   ARCHIVE=True)
add_analog("PumpOutFreq",       "REAL",     PV_DESC="Pump Output Frequency",        PV_EGU="Hz",    ARCHIVE=True)
add_analog("PumpOutVoltage",    "REAL",     PV_DESC="Pump Output Voltage",          PV_EGU="V",     ARCHIVE=True)
add_analog("PumpOutCurrent",    "REAL",     PV_DESC="Pump Output Current",          PV_EGU="A",     ARCHIVE=True)
add_analog("PumpOutPower",      "REAL",     PV_DESC="Pump Output Power",            PV_EGU="kW",    ARCHIVE=True)
add_analog("PumpOutTemp",       "REAL",     PV_DESC="Pump Power Unit Temp",         PV_EGU="degC",  ARCHIVE=True)
add_analog("PumpSpeedRPM",      "REAL",     PV_DESC="Pump actual speed in rpm",     PV_EGU="rpm",   ARCHIVE=True)
add_analog("OperatingHours",    "REAL",     PV_DESC="Pump operating hours",         PV_EGU="h",     ARCHIVE=True)
add_digital("Accelerating",     PV_DESC="Pump Accelerating",        	PV_ONAM="Accelerating",             PV_ZNAM="NotMoving")
add_digital("Decelerating",     PV_DESC="Pump Decelerating",        	PV_ONAM="Decelerating",             PV_ZNAM="NotMoving")

#Status Word - Fault is defined in the Alarms section
add_digital("BusControlMode",   PV_DESC="Bus control enabled",  PV_ONAM="Bus_Control_ON",   PV_ZNAM="Bus_Control_OFF", ARCHIVE=True)            #STATUS_WORD.0  
add_digital("Running",          					ARCHIVE=True,       						PV_DESC="Pump Running",                 PV_ONAM="Running",                       PV_ZNAM="NotRunning")                 #STATUS_WORD.1
add_digital("AutoMode",         					ARCHIVE=True,       						PV_DESC="Pump in Auto mode",            PV_ONAM="AutoMode",                      PV_ZNAM="NotAutoMode")                #STATUS_WORD.4
add_digital("PumpMaxSpeed",     					ARCHIVE=True,       						PV_DESC="Pump runs at max speed",       PV_ONAM="PumpSpeedMax",                  PV_ZNAM="NominalState")            #STATUS_WORD.5
add_digital("Ready",            					ARCHIVE=True,       						PV_DESC="Pump Ready for Start",         PV_ONAM="Ready",                         PV_ZNAM="NotReady")                   #STATUS_WORD.6
add_digital("PumpMinSpeed",     					ARCHIVE=True,       						PV_DESC="Pump runs at min speed",       PV_ONAM="PumpMinSpeed",                  PV_ZNAM="NominalState")            #STATUS_WORD.7


#Inhibit signals (set by the PLC code, can't be changed by the OPI) 
add_digital("InhibitManual",        PV_DESC="Inhibit Manual Mode",      PV_ONAM="InhibitManual",        PV_ZNAM="AllowManual")
add_digital("InhibitForce",         PV_DESC="Inhibit Force Mode",       PV_ONAM="InhibitForce",         PV_ZNAM="AllowForce")
add_digital("AlarmLatching",        PV_DESC="Latching of alarms",       PV_ONAM="Latched",              PV_ZNAM="NotLatched")

#Interlock signals
add_digital("GroupInterlock",       PV_DESC="Group Interlock",          PV_ONAM="True",     PV_ZNAM="False")
add_digital("StartInterlock",       PV_DESC="Start Interlock",          PV_ONAM="True",     PV_ZNAM="False",   ARCHIVE=True)
add_digital("StopInterlock",        PV_DESC="Stop Interlock",           PV_ONAM="True",     PV_ZNAM="False",   ARCHIVE=True)
add_string("InterlockMsg", 39,      PV_DESC="Interlock Message",        PV_NAME="InterlockMsg")

#for OPI visualization
add_digital("EnableAutoBtn",        PV_DESC="Enable Auto Button",       PV_ONAM="True",     PV_ZNAM="False")
add_digital("EnableManualBtn",      PV_DESC="Enable Manual Button",     PV_ONAM="True",     PV_ZNAM="False")
add_digital("EnableForcedBtn",      PV_DESC="Enable Force Button",      PV_ONAM="True",     PV_ZNAM="False")
add_digital("EnableStartBtn",       PV_DESC="Enable Start Button",      PV_ONAM="True",     PV_ZNAM="False")
add_digital("EnableStopBtn",        PV_DESC="Enable Stop Button",       PV_ONAM="True",     PV_ZNAM="False")
add_digital("SetAsPrimary",         PV_DESC="SetAsPrimary property",    PV_ONAM="True",     PV_ZNAM="False")
add_digital("TwinPump",           PV_DESC="Pump has a twinpump",      PV_ONAM="True",     PV_ZNAM="False")
add_analog("DeviceColor",   "INT",  PV_DESC="Pump BlockIcon Color")

#Alarm signals
add_major_alarm("GroupAlarm",       "Group Alarm",      PV_DESC="Group Alarm",  PV_ONAM="GroupAlarm",   PV_ZNAM="NominalState")
add_minor_alarm("SPLimitActive",    "SPLimitActive",                    PV_ZNAM="NominalState")
add_major_alarm("BusControlOFF",    "BusControlOFF",    PV_DESC="Drive bus control OFF",         PV_ONAM="BusControlOFF",     PV_ZNAM="NoAlarm")
add_major_alarm("Overheated",          "Overheated",                       						PV_DESC="Drive Overheated",              PV_ONAM="Alarm",             PV_ZNAM="NoAlarm")               
add_major_alarm("Fault",               "Drive Fault",                      						PV_DESC="Drive Fault is present",        PV_ONAM="Fault",             PV_ZNAM="NoAlarm")                 #STATUS_WORD.2  
add_major_alarm("DiscrepancyAlarm",       "DiscrepancyAlarm",                   						PV_DESC="SetPoint Discrepancy",          PV_ONAM="DiscrepancyAlarm",    PV_ZNAM="SP_OK")                      
add_major_alarm("Profibus_Comm_Error",     "Profinet communication error",               						PV_DESC="HW PN Module Error",            PV_ONAM="Profinet commerror", PV_ZNAM="NominalState")
add_major_alarm("SSTriggered","SSTriggered",                               PV_ZNAM="NominalState")
add_major_alarm("DriveContactorOff",          	"Drive Contactor Off",                      					PV_DESC="DriveContactorOff",                   PV_ONAM="DriveContactorOff",        PV_ZNAM="NominalState")
add_major_alarm("MotorDisconnectorOff", "Motor Disconnector Off",   PV_DESC="MotorDisconnectorOff",                   PV_ONAM="MotorDisconnectorOff",        PV_ZNAM="NominalState")



#Discrepancy
add_analog("DiscPercentage","REAL" ,     PV_DESC="Discrepancy In Percent",                     PV_EGU="%")
add_time("DiscrepancyTime",                     PV_DESC="Discrepancy Time Interval")

#Ramping
add_analog("MaxRampUpSpeed",    "REAL",     PV_DESC="Maximum Ramping UP Speed",      PV_EGU="%/s",   ARCHIVE=True)
add_analog("MaxRampDownSpeed",  "REAL",     PV_DESC="Maximum Ramping DOWN Speed",    PV_EGU="%/s",   ARCHIVE=True)
add_analog("ActRampSpeed",      "REAL",     PV_DESC="Actual Ramping Speed",          PV_EGU="%/s",   ARCHIVE=True)
add_digital("Ramping",                      PV_DESC="Ramping Indicator",        PV_ONAM="True", PV_ZNAM="False",    ARCHIVE=True)
add_digital("RampSettingOK",                PV_DESC="Ramping can be enabled",   PV_ONAM="True", PV_ZNAM="False",    ARCHIVE=True)


#Feedbacks
add_analog("FB_Setpoint",           "REAL",     PV_DESC="FB Setpoint from HMI (SP)",        PV_EGU="%",    ARCHIVE=True)
add_analog("FB_Step",               "REAL",     PV_DESC="FB Step value for open close",     PV_EGU="%")
add_analog("FB_ForceSpeedInput",    "REAL",     PV_DESC="FB Pump forced input speed",       PV_EGU="%",    ARCHIVE=True)
add_analog("FB_ForceSpeedOutput",   "REAL",     PV_DESC="FB Pump forced output speed",      PV_EGU="%",    ARCHIVE=True)


#Ramping
add_analog("FB_RampUpTime",         "INT",      PV_DESC="Ramping Up time",                  PV_EGU="sec")
add_analog("FB_RampUpRange",        "REAL",     PV_DESC="Ramping Up range",                 PV_EGU="%")
add_analog("FB_RampDownTime",       "INT",      PV_DESC="Ramping Down time",                PV_EGU="sec")
add_analog("FB_RampDownRange",      "REAL",     PV_DESC="Ramping Down range",               PV_EGU="%")

############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                				PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              				PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",               				PV_DESC="CMD: Force Mode")
add_digital("Cmd_RampON",              				PV_DESC="Turn Ramping ON")
add_digital("Cmd_RampOFF",             				PV_DESC="Turn Ramping OFF")
add_digital("Cmd_AckAlarm",            				PV_DESC="CMD: Acknowledge Alarm")
add_digital("Cmd_Start",               				PV_DESC="CMD: Pump Start")
add_digital("Cmd_Stop",                				PV_DESC="CMD: Pump Stop")
add_digital("Cmd_ManuStart",           				PV_DESC="CMD: Manual Start")
add_digital("Cmd_ManuStop",            				PV_DESC="CMD: Manual Stop")
#add_digital("Cmd_ForceStart",          			PV_DESC="CMD: Force Start") #currently not used
#add_digital("Cmd_ForceStop",           			PV_DESC="CMD: Force Stop")  #currently not used
add_digital("Cmd_SetAsPrimary",                     PV_DESC="CMD: Set Pump as primary pump")
add_digital("Cmd_ForceInputValue",                  PV_DESC="CMD: Force Input Speed")
add_digital("Cmd_ForceOutputValue",                 PV_DESC="CMD: Force Output Speed")
add_digital("Cmd_BusControl",          				PV_DESC="CMD: Bus Control ON")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Setpoint and Manipulated value from HMI
add_analog("P_Setpoint",            "REAL",     PV_DESC="Setpoint from HMI (SP)",           PV_EGU="%")
add_analog("P_Step",                "REAL",     PV_DESC="Step value for Accel./Deccel.",    PV_EGU="%")
add_analog("P_ForceInputSpeed",     "REAL",     PV_DESC="Pump forced input speed",          PV_EGU="%")
add_analog("P_ForceOutputSpeed",    "REAL" ,    PV_DESC="Pump forced output speed",         PV_EGU="%")

#Ramping speed from the HMI
add_analog("P_RampUpTime",          "INT",      PV_DESC="Ramping UP Time",                  PV_EGU="sec")
add_analog("P_RampUpRange",         "REAL",     PV_DESC="Ramping UP Range",                 PV_EGU="%")
add_analog("P_RampDownTime",        "INT",      PV_DESC="Ramping DOWN Time",                PV_EGU="sec")
add_analog("P_RampDownRange",       "REAL",     PV_DESC="Ramping DOWN Range",               PV_EGU="%")

